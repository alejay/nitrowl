<div align="center">
    <img src="https://gitlab.com/alejay/nitrowl/-/raw/master/public/icons/128.png" alt="Nitrowl logo">
</div>

# Nitrowl
## 🦉 Introduction

Don't want to pay for a Nitro subscription just for a few emotes? 😏

The Nitrowl web extension allows you to store your favorite emotes in your browser, so you can share them at any time.

Nitrowl's interface is inspired by Discord's and has been designed to be as simple as possible. 😎

To add an emote, just give it a name and provide a link to it.  
Then, you can find this emote by entering its name in the search bar, or by finding it in your list of emotes.  
Finally, to use an emote, simply click on it. It will then be saved to your clipboard, and you can paste it into any Discord conversation.

Easy, right? 😋

## 🛠 Installation

Clone the repository, and once inside, execute the commands
```
npm install
npm run build
```
Then go to Chrome, to the URL `chrome://extensions`.
Next, enable developer mode using the button at the top right of your window.  
Finally, add the extension by clicking on "Load unpacked extension", and then selecting the `dist/` folder that was generated during the build step.

## 🤗 Usage

### Adding an emote

To access the add menu, start by clicking the "+" icon at the bottom right of the extension.  
Fill in the fields, then validate. Your new emote will be accessible by relaunching the extension.

<div align="center">
    <img src="https://i.imgur.com/g1PjfHn.png" alt="plus button">
    <img src="https://i.imgur.com/9nstMXm.png" alt="field filling">
</div>

### Searching for an emote

To search for an emote, it's very simple: just type part of its name in the search bar.

<div align="center">
    <img src="https://i.imgur.com/1rB0Q0U.png" alt="partial search">
</div>

### Deleting an emote

If you're tired of one of your emotes (yes, it happens), you can easily delete it by hovering over it and clicking on the delete icon.

<div align="center">
    <img src="https://i.imgur.com/eJECL0F.png" alt="emote deletion">
</div>

### Sharing an emote

To share an emote, simply search for it, click on it, and paste the content of the image into the Discord channel of your choice.
